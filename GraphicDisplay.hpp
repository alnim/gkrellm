#ifndef GraphicDisplay_HPP
# define GraphicDisplay_HPP

#include "IMonitorDisplay.hpp"
#include "IMonitorModule.hpp"
#include <iostream>
#include <vector>
#include <string>
#include <SDL2/SDL.h>

# define RENDERER_TYPE SDL_Renderer
# define DRAWLINE SDL_RenderDrawLine
# define DRAWPOINT SDL_RenderDrawPoint

class GraphicDisplay : public IMonitorDisplay
{
	public:
		GraphicDisplay();
		GraphicDisplay(std::string title, int width, int height);
		GraphicDisplay(GraphicDisplay const &other);
		GraphicDisplay	&operator=(GraphicDisplay const &);
		~GraphicDisplay();

		void graph(IMonitorModule *module, int xpos, int ypos, int width,
																	int height);
		void init(std::string title, int x, int y, int width, int height,
															bool fullScreen);
		void handleEvents();
		void update();
		void clean();
		void addModule(IMonitorModule *module);
		bool isRunning();

		static void drawLine(int x1, int y1, int x2, int y2);
		static void drawPoint(int x, int y);
		static void writeText(int x, int y, std::string text, int size, int r,
																		int g,
																		int b);
		static void setRendererDrawColor(int r, int g, int b, int a);
		static void setRenderTarget(SDL_Texture *texture);

	static SDL_Renderer *renderer;
	private:
		std::string _title;
		std::vector<IMonitorModule*> _modules;
		int _width;
		int _height;
		bool _isRunning;
		SDL_Window *_window;
};
#endif
