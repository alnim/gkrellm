/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SDLImage.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sgardner <stephenbgardner@gmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/07 18:50:33 by sgardner          #+#    #+#             */
/*   Updated: 2018/07/08 14:56:53 by sgardner         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SDLImage.hpp"

/*
** PUBLIC
*/

SDLImage::SDLImage(void) :
	_texture(nullptr),
	_renderer(nullptr)
{
	SDL_Rect	opt = {};

	this->_dstRect = opt;
	this->_srcRect = opt;
}

SDLImage::SDLImage(SDL_Renderer *renderer, int width, int height) :
	_renderer(renderer)
{
	SDL_Rect	opt = { 0, 0, width, height };

	this->_dstRect = opt;
	this->_srcRect = opt;
	this->_init();
}

SDLImage::SDLImage(SDL_Renderer *renderer, int width, int height, int x, int y) :
	_renderer(renderer)
{
	SDL_Rect	opt = { x, y, width, height };

	this->_dstRect = opt;
	this->_srcRect = opt;
	this->_srcRect.x = 0;
	this->_srcRect.y = 0;
	this->_init();
}

SDLImage::SDLImage(SDLImage const &obj)
{
	*this = obj;
}

SDLImage::~SDLImage(void)
{
	if (this->_texture)
		SDL_DestroyTexture(this->_texture);
}

SDLImage		&SDLImage::operator=(SDLImage const &obj)
{
	SDL_Texture	*target;

	if (this->_texture)
		SDL_DestroyTexture(this->_texture);
	this->_renderer = obj.getRenderer();
	this->_dstRect = *obj.getDstRect();
	this->_srcRect = *obj.getSrcRect();
	this->_init();
	target = SDL_GetRenderTarget(this->_renderer);
	SDL_SetRenderTarget(this->_renderer, this->_texture);
	SDL_RenderCopy(
		this->_renderer,
		obj.getTexture(),
		nullptr,
		nullptr);
	SDL_SetRenderTarget(this->_renderer, target);
	return (*this);
}

void			SDLImage::clear(void)
{
	SDL_Texture	*target;

	target = SDL_GetRenderTarget(this->_renderer);
	SDL_SetRenderTarget(this->_renderer, this->_texture);
	SDL_RenderClear(this->_renderer);
	SDL_SetRenderTarget(this->_renderer, target);
}

void			SDLImage::draw(void)
{
	SDL_RenderCopy(
		this->_renderer,
		this->_texture,
		this->getSrcRect(),
		this->getDstRect());
}

SDL_Texture		*SDLImage::getTexture(void) const
{
	return (this->_texture);
}

SDL_Renderer	*SDLImage::getRenderer(void) const
{
	return (this->_renderer);
}

void			SDLImage::setRenderer(SDL_Renderer *renderer)
{
	this->_renderer = renderer;
}

// SRC Rect

SDL_Rect const	*SDLImage::getSrcRect(void) const
{
	return (&this->_srcRect);
}

int				SDLImage::getWidth(void) const
{
	return (this->_srcRect.w);
}

int				SDLImage::getHeight(void) const
{
	return (this->_srcRect.h);
}

// DST Rect

SDL_Rect const	*SDLImage::getDstRect(void) const
{
	return (&this->_dstRect);
}

int				SDLImage::getRenderWidth(void) const
{
	return (this->_dstRect.w);
}

void			SDLImage::setRenderWidth(int width)
{
	this->_dstRect.w = width;
}

int				SDLImage::getRenderHeight(void) const
{
	return (this->_dstRect.h);
}

void			SDLImage::setRenderHeight(int height)
{
	this->_dstRect.h = height;
}

int				SDLImage::getX(void) const
{
	return (this->_dstRect.x);
}

void			SDLImage::setX(int x)
{
	this->_dstRect.x = x;
}

int				SDLImage::getY(void) const
{
	return (this->_dstRect.y);
}

void			SDLImage::setY(int y)
{
	this->_dstRect.y = y;
}

/*
** PRIVATE
*/

void			SDLImage::_init(void)
{
	this->_texture = SDL_CreateTexture(
		this->_renderer,
		SDL_PIXELFORMAT_RGBA32,
		SDL_TEXTUREACCESS_TARGET,
		this->_srcRect.w,
		this->_srcRect.h);
}
