#include "IMonitorDisplay.hpp"

IMonitorDisplay::IMonitorDisplay(void)
{
	return;
}

IMonitorDisplay::~IMonitorDisplay(void)
{
	return;
}

IMonitorDisplay::IMonitorDisplay(IMonitorDisplay const &other)
{
	*this = other;
	return;
}

IMonitorDisplay	&IMonitorDisplay::operator=(IMonitorDisplay const &)
{
	return (*this);
}
