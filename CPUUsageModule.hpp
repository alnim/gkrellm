#ifndef CPUUSAGEMODULE_H
# define CPUUSAGEMODULE_H

#include "IMonitorModule.hpp"
#include <vector>
#include <iostream>
#include <sstream>
#include <string>

class CPUUsageModule : public IMonitorModule
{
public:
	CPUUsageModule(void);
	CPUUsageModule(CPUUsageModule const &other);
	CPUUsageModule	&operator=(CPUUsageModule const &other);
	CPUUsageModule(std::string const name);
	virtual ~CPUUsageModule(void);

	void							update(void);
	void							displayGraphic(void);
	void							displayText(void){};
	const std::string				readStatsFile(void) const;
	void							addFileInfoToStats(const std::string
																	&fileInfo);
	std::vector<std::string> const	&getStats(void) const;
	std::string const				&getName(void) const;
	int const						&getSize(void) const;

private:
	std::vector<std::string>		_stats;
	std::vector<std::string>		_cpuInfoStats;
	std::string						_name;
	int								_size;
	static const std::string		_monitorFilePath;
	static const std::string		_getInfoSysCommand;
};

#endif
