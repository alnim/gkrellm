#include "CPUInfoModule.hpp"

CPUInfoModule::CPUInfoModule(std::string const name) :
	IMonitorModule(), _stats(), _name(name), _size(6)
{
	char buffer[256];
	buffer[255] = '\0';
	int cores;
	int threads;
	size_t size = 255;
	sysctlbyname("machdep.cpu.brand_string", &buffer, &size, NULL, 0);
	sysctlbyname("machdep.cpu.core_count", &cores, &size, NULL, 0);
	sysctlbyname("machdep.cpu.thread_count", &threads, &size, NULL, 0);
	std::string info = buffer;
	int i = 0;
	size_t pos = 0;
	std::string delimiter = " ";
	std::string token;
	while ((pos = info.find(delimiter)) != std::string::npos) {
	    token = info.substr(0, pos);
	    if (token.compare("@") != 0 && token.compare("CPU") != 0)
	    {
	    	this->_stats.push_back(token);
			i++;
		}
	    info.erase(0, pos + delimiter.length());
	}
	this->_stats.push_back(info); //speed
	this->_stats.push_back(std::to_string(cores)); // cores
	this->_stats.push_back(std::to_string(threads)); // threads
	i += 3;
	this->_size = i;
	return ;
}

CPUInfoModule::CPUInfoModule(CPUInfoModule const &other)
{
	*this = other;
	return;
}

CPUInfoModule	&CPUInfoModule::operator=(CPUInfoModule const &)
{
	return (*this);
}

CPUInfoModule::~CPUInfoModule()
{
	return ;
}

std::vector<std::string> const		&CPUInfoModule::getStats(void) const
{
	return (this->_stats);
}

std::string const					&CPUInfoModule::getName(void) const
{
	return (this->_name);
}

void								CPUInfoModule::update(void)
{
	return;
}

int const							&CPUInfoModule::getSize(void) const
{
	return (this->_size);
}
