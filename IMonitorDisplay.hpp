#ifndef IMonitorDisplay_HPP
# define IMonitorDisplay_HPP

#include <vector>
#include <string>

#include "IMonitorModule.hpp"

class IMonitorDisplay
{
	public:
		IMonitorDisplay(void);
		virtual ~IMonitorDisplay(void);
		IMonitorDisplay(IMonitorDisplay const &other);
		IMonitorDisplay	&operator=(IMonitorDisplay const &other);

		virtual void	update(void) = 0;
		virtual void	addModule(IMonitorModule *module) = 0;
		virtual bool	isRunning() = 0;
		virtual void	clean() = 0;
};

#endif
