#include "OSInfoModule.hpp"
#include "GraphicDisplay.hpp"

OSInfoModule::OSInfoModule(std::string const name) :
	IMonitorModule(), _stats(), _name(name), _size(2)
{
    struct utsname uts;
    uname(&uts);

	this->_stats.push_back(uts.sysname);
	this->_stats.push_back(uts.release);
	return ;
}

OSInfoModule::OSInfoModule(OSInfoModule const &other)
{
	*this = other;
	return;
}

OSInfoModule	&OSInfoModule::operator=(OSInfoModule const &)
{
	return (*this);
}

OSInfoModule::~OSInfoModule()
{
	return ;
}

std::vector<std::string> const		&OSInfoModule::getStats(void) const
{
	return (this->_stats);
}

std::string const					&OSInfoModule::getName(void) const
{
	return (this->_name);
}

void								OSInfoModule::update(void)
{
	return;
}

int const							&OSInfoModule::getSize(void) const
{
	return (this->_size);
}

void								OSInfoModule::displayGraphic(void)
{
	this->_image->clear();
	GraphicDisplay::setRenderTarget(this->_image->getTexture());
	GraphicDisplay::writeText(0, 0,"System name: " + this->_stats[0], 24,0,0,255);
	GraphicDisplay::writeText(0, 24,"Release: " + this->_stats[1], 24,0,0,255);
	GraphicDisplay::setRenderTarget(nullptr);
	this->_image->draw();
}
