#include "HostnameModule.hpp"
#include "GraphicDisplay.hpp"
HostnameModule::HostnameModule(std::string const name) :
	IMonitorModule(), _stats(), _name(name), _size(2)
{
	char hn[256];
	char un[256];
	hn[255] = '\0';
	un[255] = '\0';
	gethostname(hn, 255);
	getlogin_r(un, 255);
	this->_stats.push_back(hn);
	this->_stats.push_back(un);
	return ;
}

HostnameModule::HostnameModule(HostnameModule const &other)
{
	*this = other;
	return;
}

HostnameModule	&HostnameModule::operator=(HostnameModule const &)
{
	return (*this);
}

HostnameModule::~HostnameModule()
{
	return ;
}

std::vector<std::string> const		&HostnameModule::getStats(void) const
{
	return (this->_stats);
}

std::string const					&HostnameModule::getName(void) const
{
	return (this->_name);
}

void								HostnameModule::update(void)
{
	return;
}

int const							&HostnameModule::getSize(void) const
{
	return (this->_size);
}

void								HostnameModule::displayGraphic(void)
{
	this->_image->clear();
	GraphicDisplay::setRenderTarget(this->_image->getTexture());
	GraphicDisplay::writeText(0, 0,"Hostname: " + this->_stats[0], 24,0,0,255);
	GraphicDisplay::writeText(0, 24,"Login: " + this->_stats[1], 24,0,0,255);
	GraphicDisplay::setRenderTarget(nullptr);
	this->_image->draw();
}
