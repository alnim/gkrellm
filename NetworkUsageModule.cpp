#include "NetworkUsageModule.hpp"
#include "GraphicDisplay.hpp"

NetworkUsageModule::NetworkUsageModule(std::string const name) :
	IMonitorModule(), _stats(), _name(name), _size(4)
{
	system("mkdir monitorFiles >/dev/null 2>/dev/null");
	this->update();
	return ;
}

NetworkUsageModule::~NetworkUsageModule()
{
	return ;
}

NetworkUsageModule::NetworkUsageModule(NetworkUsageModule const &other)
{
	*this = other;
	return;
}

NetworkUsageModule					&NetworkUsageModule::operator=(NetworkUsageModule const &)
{
	return (*this);
}

std::vector<std::string> const		&NetworkUsageModule::getStats(void) const
{
	return (this->_stats);
}

std::string const					&NetworkUsageModule::getName(void) const
{
	return (this->_name);
}

void								NetworkUsageModule::update(void)
{
	if(this->_stats.size() > 100)
		this->_stats.clear();
	system((NetworkUsageModule::_getInfoSysCommand).c_str());
	this->addFileInfoToStats(this->readStatsFile());
	return;
}

int const							&NetworkUsageModule::getSize(void) const
{
	return (this->_size);
}

const std::string					NetworkUsageModule::readStatsFile(void) const
{
	std::ifstream file;
	file.open(NetworkUsageModule::_monitorFilePath);
	std::string line = "";
	if (file.is_open())
	{
		std::getline(file, line);
		file.close();
	}
	else
	{
		line = "User: 0 | Sys: 0 | Idle: 0";
	}
	return line;
}

void								NetworkUsageModule::addFileInfoToStats(std::string const info)
{
	std::string token;
	size_t pos = info.find("packets: ") + 9;
	size_t end = info.find("/") - 1;
	token = info.substr(pos, end - pos);
	this->_stats.push_back(token); // packets in

	pos = info.find("/", end) + 1;
	end = info.find(" in", pos);
	token = info.substr(pos, end - pos);
	this->_stats.push_back(token); // in size

	pos = info.find(", ", end) + 2;
	end = info.find("/", pos) - 1;
	token = info.substr(pos, end - pos);
	this->_stats.push_back(token); // packets out

	pos = info.find("/", end) + 1;
	end = info.find(" out", pos);
	token = info.substr(pos, end - pos);
	this->_stats.push_back(token); // out size
}

void							NetworkUsageModule::displayGraphic(void)
{
	this->_image->clear();
	GraphicDisplay::setRenderTarget(this->_image->getTexture());
	GraphicDisplay::writeText(0, 24 * 0,"Network Usage: ", 24,0,0,255);
	GraphicDisplay::writeText(0, 24 * 1,"Packets In: " + this->_stats.at(this->_stats.size() - 4), 24,0,0,255);
	GraphicDisplay::writeText(0, 24 * 2,"Size of Packets In: " + this->_stats.at(this->_stats.size() - 3), 24,0,0,255);
	GraphicDisplay::writeText(0, 24 * 3,"Packets Out: " + this->_stats.at(this->_stats.size() - 2), 24,0,0,255);
	GraphicDisplay::writeText(0, 24 * 4,"Size of Packets Out: " + this->_stats.at(this->_stats.size() - 1), 24,0,0,255);
	GraphicDisplay::setRenderTarget(nullptr);
	this->_image->draw();
}

const std::string NetworkUsageModule::_monitorFilePath = "monitorFiles/netUsage.monitor";
const std::string NetworkUsageModule::_getInfoSysCommand = "top -l 1 | grep \"Networks:\" > " + NetworkUsageModule::_monitorFilePath;
