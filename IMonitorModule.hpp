#ifndef IMONITORMODULE_HPP
# define IMONITORMODULE_HPP

#include <vector>
#include <string>
#include "SDLImage.hpp"

class IMonitorModule
{
	public:
		IMonitorModule(void);
		virtual ~IMonitorModule(void);
		IMonitorModule(IMonitorModule const &other);
		IMonitorModule	&operator=(IMonitorModule const &other);

		virtual void							update(void) = 0;
		virtual void							displayGraphic(void) = 0;
		virtual void							displayText(void) = 0;
		virtual std::vector<std::string> const	&getStats(void) const = 0;
		virtual std::string const				&getName(void) const = 0;
		virtual int const						&getSize(void) const = 0;
		SDLImage						*getImage();
		void							setImage(SDLImage *image);
	protected:
		SDLImage						*_image;
};

#endif
