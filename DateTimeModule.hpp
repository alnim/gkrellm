#ifndef DATETIMEMODULE_H
# define DATETIMEMODULE_H

#include "IMonitorModule.hpp"
#include <ctime>
#include <vector>
#include <iostream>
#include <sstream>
#include <string>

class DateTimeModule : public IMonitorModule
{
public:
	DateTimeModule(void);
	DateTimeModule(DateTimeModule const &other);
	DateTimeModule	&operator=(DateTimeModule const &other);
	DateTimeModule(std::string const name);
	virtual ~DateTimeModule(void);

	void							update(void);
	void							displayGraphic(void);
	void							displayText(void){};
	std::vector<std::string> const	&getStats(void) const;
	std::string const				&getName(void) const;
	int const						&getSize(void) const;

private:
	std::vector<std::string>		_stats;
	std::string						_name;
	int								_size;
};

std::string resize(std::string str);

#endif
