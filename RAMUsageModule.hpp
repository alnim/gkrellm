#ifndef RAMUSAGEMODULE_H
# define RAMUSAGEMODULE_H

#include "IMonitorModule.hpp"
#include <vector>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>

class RAMUsageModule : public IMonitorModule
{
public:
	RAMUsageModule(void);
	RAMUsageModule(RAMUsageModule const &other);
	RAMUsageModule	&operator=(RAMUsageModule const &other);
	RAMUsageModule(std::string const name);
	virtual ~RAMUsageModule(void);

	void							update(void);
	void							displayGraphic(void);
	void							displayText(void){};
	const std::string				readStatsFile(void) const;
	void							addFileInfoToStats(std::string fileInfo);
	std::vector<std::string> const	&getStats(void) const;
	std::string const				&getName(void) const;
	int const						&getSize(void) const;

private:
	std::vector<std::string>		_stats;
	std::string						_name;
	int								_size;
	static const std::string		_monitorFilePath;
	static const std::string		_getInfoSysCommand;
};

#endif
