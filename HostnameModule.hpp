#ifndef HOSTNAMEMODULE_H
# define HOSTNAMEMODULE_H

#include "IMonitorModule.hpp"
#include <unistd.h>
#include <vector>
#include <iostream>
#include <string>

class HostnameModule : public IMonitorModule
{
public:
	HostnameModule(void);
	HostnameModule(HostnameModule const &other);
	HostnameModule	&operator=(HostnameModule const &other);
	HostnameModule(std::string const name);
	virtual ~HostnameModule(void);

	void							update(void);
	void							displayGraphic(void);
	void							displayText(void){};
	std::vector<std::string> const	&getStats(void) const;
	std::string const				&getName(void) const;
	int const						&getSize(void) const;

private:
	std::vector<std::string>		_stats;
	std::string						_name;
	int								_size;
};

#endif
