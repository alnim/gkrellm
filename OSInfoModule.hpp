#ifndef OSINFOMODULE_H
# define OSINFOMODULE_H

#include "IMonitorModule.hpp"
#include <iostream>
#include <vector>
#include <string>
#include <sys/utsname.h>

class OSInfoModule : public IMonitorModule
{
public:
	OSInfoModule(void);
	OSInfoModule(OSInfoModule const &other);
	OSInfoModule	&operator=(OSInfoModule const &other);
	OSInfoModule(std::string const name);
	virtual ~OSInfoModule(void);

	void							update(void);
	void							displayGraphic(void);
	void							displayText(void){};
	std::vector<std::string> const	&getStats(void) const;
	std::string const				&getName(void) const;
	int const						&getSize(void) const;

private:
	std::vector<std::string>		_stats;
	std::string						_name;
	int								_size;
};

#endif
