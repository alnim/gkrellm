#include "CPUUsageModule.hpp"
#include "GraphicDisplay.hpp"
# include <sys/sysctl.h>
#include <fstream>
const std::string CPUUsageModule::_monitorFilePath = "monitorFiles/cpuUsage.monitor";
const std::string CPUUsageModule::_getInfoSysCommand =
"top -l 1 | grep \"CPU usage:\" > " + CPUUsageModule::_monitorFilePath;

CPUUsageModule::CPUUsageModule(std::string const name) :
	IMonitorModule(), _stats(), _name(name), _size(3)
{
	system("mkdir monitorFiles >/dev/null 2>/dev/null");
	char buffer[256];
	buffer[255] = '\0';
	int cores;
	int threads;
	size_t size = 255;
	sysctlbyname("machdep.cpu.brand_string", &buffer, &size, NULL, 0);
	sysctlbyname("machdep.cpu.core_count", &cores, &size, NULL, 0);
	sysctlbyname("machdep.cpu.thread_count", &threads, &size, NULL, 0);
	std::string info = buffer;
	int i = 0;
	size_t pos = 0;
	std::string delimiter = " ";
	std::string token;
	while ((pos = info.find(delimiter)) != std::string::npos) {
		token = info.substr(0, pos);
		if (token.compare("@") != 0 && token.compare("CPU") != 0)
		{
			this->_cpuInfoStats.push_back(token);
			i++;
		}
		info.erase(0, pos + delimiter.length());
	}
	this->_cpuInfoStats.push_back(info); //speed
	this->_cpuInfoStats.push_back(std::to_string(cores)); // cores
	this->_cpuInfoStats.push_back(std::to_string(threads)); // threads
	i += 3;
	this->_size = i;
	this->update();
	return ;
}

CPUUsageModule::CPUUsageModule(CPUUsageModule const &other)
{
	*this = other;
	return;
}

CPUUsageModule	&CPUUsageModule::operator=(CPUUsageModule const &)
{
	return (*this);
}

CPUUsageModule::~CPUUsageModule()
{
	return ;
}

std::vector<std::string> const		&CPUUsageModule::getStats(void) const
{
	return (this->_stats);
}

std::string const					&CPUUsageModule::getName(void) const
{
	return (this->_name);
}

int const							&CPUUsageModule::getSize(void) const
{
	return (this->_size);
}

void								CPUUsageModule::update(void)
{
	//if(this->_stats.size() > 100)
	//	this->_stats.clear();
	//System call to get the CPU Usage from top
	system((CPUUsageModule::_getInfoSysCommand).c_str());
	/*Add the proper percentages for user, idle, sys, respectively to the
	  stats vector */
	this->addFileInfoToStats(this->readStatsFile());
	return;
}

const std::string					CPUUsageModule::readStatsFile(void) const
{
	std::ifstream monitorFile;
	monitorFile.open(CPUUsageModule::_monitorFilePath);
	if (!monitorFile.is_open()) {
		//Make this an actual error...
		std::cout << "File opening error." << std::endl;
		exit(1);
	}
	std::string fileLine;
	std::getline(monitorFile, fileLine);
	monitorFile.close();
	return fileLine;
}
void								CPUUsageModule::addFileInfoToStats(
		const std::string &fileInfo)
{
	std::string token;
	size_t pos = fileInfo.find(": ") + 2;
	size_t end = fileInfo.find("% user");
	token = fileInfo.substr(pos, end - pos);
	this->_stats.push_back(token); // user

	pos = fileInfo.find(", ", end) + 2;
	end = fileInfo.find("% sys", pos);
	token = fileInfo.substr(pos, end - pos);
	this->_stats.push_back(token); // sys

	pos = fileInfo.find(", ", end) + 2;
	end = fileInfo.find("% idle", pos);
	token = fileInfo.substr(pos, end - pos);
	this->_stats.push_back(token); // idle
}

void								CPUUsageModule::displayGraphic(void)
{
	this->_image->clear();
	GraphicDisplay::setRenderTarget(this->_image->getTexture());
	GraphicDisplay::setRendererDrawColor(0,0,0,255);
	int graphWidth = this->_image->getWidth();
	int graphHeight = this->_image->getHeight() - 108 - 80;
	//draw axis
	int xOffset = 0;
	int yOffset = 144 + 24;
	//Draw Past Ten Points
	std::cout << "In here" << std::endl;
	int statsSize = this->_stats.size();
	int pX[3] = {0,0,0};
	int pY[3] = {0,0,0};
	GraphicDisplay::writeText(0,0,"- CPU - ", 24,0,0,0);
	GraphicDisplay::writeText(0, 18 * (1 + 1),"" + this->_cpuInfoStats[0], 18,0,0,0);
	GraphicDisplay::writeText(0, 18 * (2 + 1),"" + this->_cpuInfoStats[1], 18,0,0,0);
	GraphicDisplay::writeText(0, 18 * (3 + 1),"" + this->_cpuInfoStats[2], 18,0,0,0);
	GraphicDisplay::writeText(0, 18 * (4 + 1),"Clockrate: " + this->_cpuInfoStats[3], 18,0,0,0);
	GraphicDisplay::writeText(0, 18 * (5 + 1),"# of Cores" + this->_cpuInfoStats[4], 18,0,0,0);
	GraphicDisplay::writeText(0, 18 * (6 + 1),"Threads: " + this->_cpuInfoStats[5], 18,0,0,0);
	GraphicDisplay::writeText(0 , 18 * (7 + 1),"User: " + this->_stats[statsSize - 1] + "%", 18,255,0,0);
	GraphicDisplay::writeText(1 * graphWidth / 3, 18 * (7 + 1),"Sys: " + this->_stats[statsSize - 2] + "%", 18,0,255,0);
	GraphicDisplay::writeText(2 * graphWidth / 3,18 * (7 + 1),"Idle: " + this->_stats[statsSize - 3] + "%", 18,0,0,255);
	for(int i = 0; (i < 30) && (i < statsSize); i+=3){
		for(int j= 0; j < 3; j++){
			if(j == 0)
				GraphicDisplay::setRendererDrawColor(255,0,0,255);
			else if(j == 1)
				GraphicDisplay::setRendererDrawColor(0,255,0,255);
			else if(j == 2)
				GraphicDisplay::setRendererDrawColor(0,0,255,255);

			if(i == 0){
				pX[j] = xOffset + 0 + (i/3 * 20);
				pY[j] = yOffset + (std::stof(this->_stats[(statsSize - 1 - j) - i]) *
						graphHeight / 100);
				GraphicDisplay::drawPoint(pX[j], pY[j]);
			}
			else
			{
				GraphicDisplay::drawLine(pX[j], pY[j], xOffset +  0 + (i/3 * graphWidth / 10), yOffset +
						(std::stof(this->_stats[(statsSize - 1 - j) - i]) * graphHeight / 100));
				pX[j] = xOffset +  0 + (i/3 * graphWidth / 10);
				pY[j] = yOffset + (std::stof(this->_stats[(statsSize - 1 - j) - i]) *
						graphHeight / 100);
			}
		}
	}
	GraphicDisplay::drawLine(xOffset, yOffset + graphHeight, 0 + graphWidth, yOffset + 0 + graphHeight);
	GraphicDisplay::drawLine(xOffset, yOffset, xOffset, yOffset + graphHeight);
	GraphicDisplay::setRenderTarget(nullptr);
	this->_image->draw();
}
